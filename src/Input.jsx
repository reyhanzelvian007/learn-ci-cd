import { Component } from "react";

class Input extends Component {
    constructor({label, placeholder}) {
        super();
        this.state = {
            angka: 0
        };
    }

    render() {
        const {label, placeholder} = this.props;
        return (
            <>
                {this.state.angka}
                <button onClick={() => this.setState({angka: this.state.angka + 1})}>Tambah</button>
                <label>{label}</label>
                <input type="text" id="inputNama" placeholder={placeholder} />
            </>
        );
    }
}

export default Input;