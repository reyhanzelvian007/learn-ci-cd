import './App.css';
import Input from './Input';
import Card from './components/Card/Card';
import Kartu from './components/Kartu/Kartu';
import Teks from './components/Teks/Teks';
import {useState} from 'react';

function App() {
  const [isShow, setIsShow] = useState(true);
  /* eslint-disable */
  const [nama, setNama] = useState("Reyhan");

  return (
    <>
      <h1 className='App'>Hello Gais!</h1>
      <h1>Ruhul jelek</h1>
      <h1>Tambahan hehe</h1>
      <p>{nama}</p>
      {isShow ? <Kartu /> : null}
      <Kartu />
      <button onClick={() => setIsShow(false)}>Hide</button>
      <Teks />
      <Card name="Reyhan" status="Mahasiswa"/>
      <Card name="Zelvian" status="Pengangguran"/>
      <Card name="Muhammad" status="Atlet"/>
      <Input label="Input Nama" placeholder="Masukkan nama">
        <p>Mwehehehe</p>
      </Input>
    </>  
  );
}

export default App;
