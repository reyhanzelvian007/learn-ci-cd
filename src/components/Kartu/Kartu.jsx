import { Component } from "react";
import style from "./Kartu.module.css";

class Kartu extends Component {
    componentDidMount() {
        document.body.style.backgroundColor = "salmon";
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }

    componentWillUnmount() {
        document.body.style.backgroundColor = "white";
    }
   
    render() {
        return (
            <>
                <section className={style["kartu-wrapper"]}>
                    <h1 className={style.title}>Reyhan</h1>
                </section>
            </>
        )
    }
}

export default Kartu;