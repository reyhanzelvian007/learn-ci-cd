const Text = ({ type, data }) => {
    // if (type === "h1") {
    //     return (
    //         <>
    //             <h1>{data}</h1>
    //         </>
    //     )
    // } else {
    //     return (
    //         <>
    //             <p>{data}</p>
    //         </>
    //     )
    // }
    return (
        <>
            {type === "h1" ? <h1>{data}</h1> : <p>{data}</p>}
        </>
    )
}

export default Text;