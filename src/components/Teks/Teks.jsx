// import style from "./Teks.module.css";
import { Component } from "react";

// 3 Fase Lifecycle
// 1. Mounting
// 2. Updating
// 3. Unmounting

class Teks extends Component {
    render() {
        return (
            <h1>{this.props.content}</h1>
        );
    }
}

export default Teks;