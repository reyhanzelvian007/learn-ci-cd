import { Component } from "react";
import Text from "../Text/Text";

class Card extends Component {
    constructor() {
        super();
        this.state = {
            statusHire: false
        };
    }
    render() {
        return (
            <>
                <section className="">
                    <Text type="h1" data={this.props.name} />
                    <Text type="p" data={this.props.status} />
                    <button
                        onClick={() => this.setState({statusHire: true})}
                        disabled={this.state.statusHire ? true : false}
                    >
                        {!this.state.statusHire ? "Hiring" : "Sudah di hire"}
                    </button>
                </section>
            </>
        )
    }
}

export default Card;